# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#   
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================

import ROOT  

def main():

  inputFile = ROOT.TFile.Open("input_file.root")

  truth = inputFile.Get("truthnom")
  reco = inputFile.Get("reconom")
  reco_bkg = inputFile.Get("reco_bkgnom")
  resp = inputFile.Get("responsenom")

  truth_test = inputFile.Get("truthnom_test")
  data = inputFile.Get("reconom_test")
  
  truth.SetDirectory(0)
  reco.SetDirectory(0)
  reco_bkg.SetDirectory(0)
  resp.SetDirectory(0)
  truth_test.SetDirectory(0)
  data.SetDirectory(0)
  
  inputFile.Close()
  
  spec = ROOT.RooUnfoldSpec("unfold","unfold",truth,"obs_truth",reco,"obs_reco",resp,reco_bkg,data,True,0.0005)

  response = spec.makeFunc(ROOT.RooUnfolding.kInvert).unfolding().response()

  pur_vec = response.Vpurity()
  eff_vec = response.Vefficiency()

  mresponse = response.Mresponse(True)
  
if __name__=="__main__":
  main()
  
