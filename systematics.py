# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#   
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================

import ROOT  
import math 

def main():

  ROOT.gRandom.SetSeed(1234)

  inputFile = ROOT.TFile.Open("input_file.root")

  truth = inputFile.Get("truthnom")
  reco = inputFile.Get("reconom")
  reco_bkg = inputFile.Get("reco_bkgnom")
  resp = inputFile.Get("responsenom")

  truth_test = inputFile.Get("truthnom_test")
  data = inputFile.Get("reconom_test")

  response_up = inputFile.Get("responseNP2up")
  response_down = inputFile.Get("responseNP2down")

  bkg_up = inputFile.Get("reco_bkgNP4up")
  bkg_down = inputFile.Get("reco_bkgNP4down")

  truth_up = inputFile.Get("truthNP5up")
  truth_down = inputFile.Get("truthNP5down")
  
  truth.SetDirectory(0)
  reco.SetDirectory(0)
  reco_bkg.SetDirectory(0)
  resp.SetDirectory(0)
  truth_test.SetDirectory(0)
  
  data.SetDirectory(0)
  bkg_up.SetDirectory(0)
  bkg_down.SetDirectory(0)

  spec = ROOT.RooUnfoldSpec("unfold","unfold",truth,"obs_truth",reco,"obs_reco",resp,reco_bkg,data,False,0.0005)

  spec.registerSystematic(ROOT.RooUnfoldSpec.kResponse, "Det_NP", response_up, response_down)  

  spec.registerSystematic(ROOT.RooUnfoldSpec.kBackground, "Bkg_NP", bkg_up, bkg_down)  

  spec.registerSystematic(ROOT.RooUnfoldSpec.kTruth, "Theory_NP", truth_up, truth_down)
  
  unfold_func = spec.makeFunc(ROOT.RooUnfolding.kPoisson,0.01)
  unfold = unfold_func.unfolding()
  
  unfold.IncludeSystematics(ROOT.RooUnfolding.kAll)
  
  unfold.SetNToys(10000)
  
  unfolded_vec = unfold.Vunfold()
  
  errors_vec = unfold.EunfoldV(ROOT.RooUnfolding.kErrorsToys)
  cov_matrix = unfold.Eunfold(ROOT.RooUnfolding.kCovToys)
  
  for i in range(unfolded_vec.GetNrows()):
    if unfolded_vec(i) > 0:
      print(str(errors_vec(i)/unfolded_vec(i)))

  inputFile.Close()
    
if __name__=="__main__":
  main()
  
