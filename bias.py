# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#   
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================

import ROOT  
import math 

def main():

  inputFile = ROOT.TFile.Open("input_file.root")

  truth = inputFile.Get("truthnom")
  reco = inputFile.Get("reconom")
  reco_bkg = inputFile.Get("reco_bkgnom")
  resp = inputFile.Get("responsenom")

  truth_test = inputFile.Get("truthnom_test")
  data = inputFile.Get("reconom_test")

  truth_alt = inputFile.Get("truthNP5down")
  
  truth.SetDirectory(0)
  reco.SetDirectory(0)
  reco_bkg.SetDirectory(0)
  resp.SetDirectory(0)
  truth_test.SetDirectory(0)
  
  data.SetDirectory(0)
  
  spec = ROOT.RooUnfoldSpec("unfold","unfold",truth,"obs_truth",reco,"obs_reco",resp,reco_bkg,data,False,0.0005)
    
  unfold_func = spec.makeFunc(ROOT.RooUnfolding.kPoisson,0.1)
  unfold = unfold_func.unfolding()
  unfolded_vec = unfold.Vunfold()
  
  unfold.CalculateBias(1000, spec.makeHistogram(truth))
  
  bias_vec = unfold.Vbias()

  unfold.CalculateBias(1000, spec.makeHistogram(truth_alt))

  bias_alt_vec = unfold.Vbias()

  for i in range(unfolded_vec.GetNrows()):
    if unfolded_vec(i) > 0:
      print("Rel. bias truth: "+str(bias_vec(i)/unfolded_vec(i))+" alt. truth: "+str(bias_alt_vec(i)/unfolded_vec(i)))

  inputFile.Close()
    
if __name__=="__main__":
  main()
  
